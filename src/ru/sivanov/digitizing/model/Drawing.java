package ru.sivanov.digitizing.model;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Vector;

import javax.imageio.ImageIO;

/**
 * Created by ivanov on 14.04.16.
 */
public class Drawing extends Observable {

	Plane plane;

	public Drawing() {
		plane = new Plane();
	}

	public void openImage(File file) {
		try {
			Image image = ImageIO.read(file);
			plane.setImage(image);
			this.setChanged();
			this.notifyObservers();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Image getImage(int width, int height) {
		return plane.getImage(width, height);
	}

	public void move(int x, int y) {
		plane.move(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	public void zoomIn(int x, int y) {
		plane.zoomIn(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	public void zoomOut(int x, int y) {
		plane.zoomOut(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	public void addPoint(int x, int y) {
		plane.addPoint(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	public Vector<Double> getScale() {
		return plane.getScale();
	}

	public void setScale(double kx, double ky) {
		plane.setScale(kx, ky);
		this.setChanged();
		this.notifyObservers();

	}

	public Point getCenter() {
		return plane.getCenter();
	}

	public void setCenter(double x, double y) {
		plane.setCenter(x, y);
		this.setChanged();
		this.notifyObservers();
	}

	public String convertToString() {
		return plane.convertToString();
	}
}
