package ru.sivanov.digitizing.model;

import java.util.Iterator;
import java.util.Vector;

/**
 * Created by ivanov on 17.04.16.
 */
public class Graph {
	Vector<Point> points = new Vector<Point>();
	Vector<Double> scale = new Vector<Double>();
	Point center = new Point(0, 0);
	Point realCenter;
	Point xLine;
	Point yLine;

	public Graph() {
		scale.add(1.0);
		scale.add(1.0);
	}

	public void addPoint(double x0, double y0) {
		Point point = new Point(x0, y0);
		if(realCenter == null) {
			realCenter = point;
		} else if(xLine == null) {
			xLine = point;
		} else if(yLine == null) {
			yLine = point;
		} else {
			points.add(point);
		}
	}

	public boolean delPoint(double x0, double y0) {
		Point p0 = new Point(x0, y0);
		double radius = 10;
		boolean successful = false;

		if(realCenter != null && p0.distanceTo(realCenter) < radius) {
			realCenter = null;
			successful = true;
		} else if(xLine != null && p0.distanceTo(xLine) < radius) {
			xLine = null;
			successful = true;

		} else if(yLine != null && p0.distanceTo(yLine) < radius) {
			yLine = null;
			successful = true;

		}
		for(Iterator<Point> iterator = points.iterator(); iterator.hasNext();) {
			Point p = iterator.next();
			if(p0.distanceTo(p) < radius) {
				iterator.remove();
				successful = true;
			}
		}
		return successful;
	}

	public Point getRealCenter() {
		return realCenter;
	}

	public Point getYLine() {
		return yLine;
	}

	public Point getXLine() {
		return xLine;
	}

	public Vector<Point> getPoints() {
		return (Vector<Point>)points.clone();
	}

	public void setScale(double kx, double ky) {
		scale.add(0, kx);
		scale.add(1, ky);
	}
	public Vector<Double> getScale() {
		return scale;
	}

	public void setCenter(double x0, double y0) {
		center = new Point(x0, y0);
	}
	public Point getCenter() {
		return center;
	}

	public String convertToString() {
		Vector<Point> realPoints = new Vector<Point>();
		if(realCenter == null || xLine == null || yLine == null) {
			return null;
		}

		Point x = xLine.sub(realCenter);
		Point y = yLine.sub(realCenter);

		for(Point p1: points) {
			Point p = p1.sub(realCenter);
			double prX = -(p.getY() * y.getX() - p.getX() * y.getY()) / (x.getX() * y.getY() - x.getY() * y.getX());
			double prY = (p.getY() * x.getX() - p.getX() * x.getY()) / (x.getX() * y.getY() - x.getY() * y.getX());
			System.out.println(prX + " " + prY);
			double newX = scale.elementAt(0) * prX + center.getX();
			double newY = scale.elementAt(1) * prY + center.getY();
			realPoints.add(new Point(newX, newY));

		}

		StringBuilder sb = new StringBuilder();
		for(Point p: realPoints) {
			sb.append("(");
			sb.append(p.getX());
			sb.append(", ");
			sb.append(p.getY());
			sb.append("),\n");
		}
		return sb.toString();
	}
}
