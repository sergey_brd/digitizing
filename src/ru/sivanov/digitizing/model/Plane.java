package ru.sivanov.digitizing.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

/**
 * Created by ivanov on 15.04.16.
 */
public class Plane {
	//x0, y0 расположение верхнего левого угла изображения относительно окна
	//k0 во сколько раз увеличено изображение
	private double x0 = 0;
	private double y0 = 0;
	private double k0 = 1;
	private double k = Math.pow(2, 1.0 / 10);
	private Image background;

	Graph graph = new Graph();

	public void setImage(Image newImage) {
		x0 = 0;
		y0 = 0;
		k0 = 1;
		background = newImage;
	}

	private void drawPoint(Graphics graphics, Point p, Color color) {
		int radius = 2;
		int x = toScreenX(p.getX());
		int y = toScreenY(p.getY());
		graphics.setColor(color);
		graphics.fillOval(x - radius, y - radius, 2 * radius, 2 * radius);
	}

	public Image getImage(int width, int height) {
		Image image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = image.getGraphics();
		if(background != null) {
			graphics.drawImage(background, (int)x0, (int)y0, (int)(k0 * background.getWidth(null)), (int)(k0 * background.getHeight(null)), null);
		}
		drawPlane(image);
		graphics.setColor(Color.RED);

		Point tmp;
		if((tmp = graph.getRealCenter()) != null) {
			drawPoint(graphics, tmp, Color.GREEN);
		}
		if((tmp = graph.getXLine()) != null) {
			drawPoint(graphics, tmp, Color.GREEN);
		}
		if((tmp = graph.getYLine()) != null) {
			drawPoint(graphics, tmp, Color.GREEN);
		}
		for(Point p: graph.getPoints()) {
			drawPoint(graphics, p, Color.RED);

		}
		return image;
	}

	public void move(int x, int y) {
		x0 += x;
		y0 += y;
	}

	public void zoomIn(int x, int y) {
		x0 += (1 - k) * (x - x0);
		y0 += (1 - k) * (y - y0);
		k0 *= k;

	}

	public void zoomOut(int x, int y) {
		x0 += (1 - 1/k) * (x - x0);
		y0 += (1 - 1/k) * (y - y0);
		k0 /= k;
	}

	private void drawPlane(Image abstractCanvas) {
		int w = abstractCanvas.getWidth(null) - 1;
		int h = abstractCanvas.getHeight(null) - 1;
		int grid = 100;

		double step = Math.log10(grid / k0);
		int tmp = (int) Math.floor(step);
		if (step >= tmp + Math.log10(5)) {
			step = Math.pow(10, tmp) * 5;
		} else if (step >= tmp + Math.log10(2)) {
			step = Math.pow(10, tmp) * 2;
		} else {
			step = Math.pow(10, tmp);
		}
		Graphics g = abstractCanvas.getGraphics();
		g.setColor(Color.BLACK);

		NumberFormat formatter = new DecimalFormat("0");

		double showX = Math.ceil(toPlaneX(0) / step) * step;
		for (double j = showX; j < toPlaneX(w); j += step) {
			int i = toScreenX(j);
			g.drawLine(i, 0, i, h);
			g.drawString(formatter.format(j), i, h);

		}

		double showY = Math.ceil(toPlaneY(0) / step) * step;
		for (double j = showY; j < toPlaneY(h); j += step) {
			int i = toScreenY(j);
			g.drawLine(0, i, w, i);
			g.drawString(formatter.format(j), 1, i);
		}
	}

	private int toScreenX(double x) {
		return (int)(x0 + (x ) * k0);
	}

	private double toPlaneX(double x) {
		return ((x - x0) / k0 );
	}

	private int toScreenY(double y) {
		return (int)(y0 + (y) * k0);
	}

	private double toPlaneY(double y) {
		return ((y - y0) / k0);
	}

	public void addPoint(int x0, int y0) {
		double x = toPlaneX(x0);
		double y = toPlaneY(y0);
		if(!graph.delPoint(x, y)) {
			graph.addPoint(x, y);
		}
	}

	public Vector<Double> getScale() {
		return graph.getScale();
	}

	public void setScale(double kx, double ky) {
		graph.setScale(kx, ky);
	}

	public Point getCenter() {
		return graph.getCenter();
	}

	public void setCenter(double x, double y) {
		graph.setCenter(x, y);
	}

	public String convertToString() {
		return graph.convertToString();
	}
}
