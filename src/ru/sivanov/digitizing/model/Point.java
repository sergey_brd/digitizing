package ru.sivanov.digitizing.model;

/**
 * Created by ivanov on 18.04.16.
 */
public class Point {
	private double x;
	private double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double distanceTo(Point p) {
		return Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));
	}

	public Point sub(Point p) {
		return new Point(x - p.x, y - p.y);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
