package ru.sivanov.digitizing.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ru.sivanov.digitizing.model.Drawing;
import ru.sivanov.digitizing.model.Point;

/**
 * Created by ivanov on 14.04.16.
 */
public class View extends JFrame implements Observer {
	private Drawing drawing = null;
	private JPanel canvas = null;
	private int x0;
	private int y0;

	public View() {

		drawing = new Drawing();
		drawing.addObserver(this);
		JMenuBar jMenuBar = createJMenuBar();
		this.setJMenuBar(jMenuBar);


		JPanel canvas = new JPanel() {
			public void paintComponent(Graphics g) {
				if(drawing != null) {
					Image image = drawing.getImage(this.getWidth(), this.getHeight());
					if(image != null) {
						g.drawImage(image, 0, 0, null);
					}
				}
			}
		};
		this.add("Center", canvas);
		canvas.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				x0 = mouseEvent.getX();
				y0 = mouseEvent.getY();
				drawing.addPoint(x0, y0);
			}

			@Override
			public void mousePressed(MouseEvent mouseEvent) {
				x0 = mouseEvent.getX();
				y0 = mouseEvent.getY();
			}

			@Override
			public void mouseReleased(MouseEvent mouseEvent) {

			}

			@Override
			public void mouseEntered(MouseEvent mouseEvent) {

			}

			@Override
			public void mouseExited(MouseEvent mouseEvent) {

			}
		});
		canvas.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent mouseEvent) {
				super.mouseDragged(mouseEvent);
				drawing.move(-x0 + mouseEvent.getX(), -y0 + mouseEvent.getY());
				x0 = mouseEvent.getX();
				y0 = mouseEvent.getY();

			}
		});
		canvas.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
				int step = mouseWheelEvent.getWheelRotation();
				if (step > 0) {
					for (int i = 0; i < step; i++) {
						drawing.zoomIn(mouseWheelEvent.getX(), mouseWheelEvent.getY());
					}
				} else {
					for (int i = 0; i < -step; i++) {
						drawing.zoomOut(mouseWheelEvent.getX(), mouseWheelEvent.getY());
					}
				}
			}
		});

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this.setMinimumSize(new Dimension(800, 600));
		this.setSize(new Dimension(800, 600));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		this.repaint();
	}

	double k[] = new double[4];

	private Object[] createScaleMenu() {
		String[] labels = {"X scale: ", "Y scale: ", "X center coord", "Y center coord"};
		int numPairs = labels.length;
		Vector<Double> scale = drawing.getScale();
		Point center = drawing.getCenter();
		JPanel panels[] = new JPanel[4];
		k[0] = scale.elementAt(0);
		k[1] = scale.elementAt(1);
		k[2] = center.getX();
		k[3] = center.getY();
		for(int i = 0; i < numPairs; i++) {
			panels[i]= new JPanel(new BorderLayout());
			panels[i].setBorder(BorderFactory.createTitledBorder(labels[i]));
			SpinnerNumberModel model = new SpinnerNumberModel(k[i], -1000, 1000, 0.1);
			JSpinner spinner = new JSpinner(model);
			final Integer number = i;
			spinner.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					k[number] = (double)spinner.getValue();
				}
			});
			panels[i].add(spinner);
		}
		return panels;

	}

	private Object[] createTextArea() {
		JPanel panels[] = new JPanel[1];
		JTextArea textArea= new JTextArea(15, 30);
		textArea.setText(drawing.convertToString());
		JScrollPane scrollPane = new JScrollPane(textArea);
		panels[0] = new JPanel();
		panels[0].add(scrollPane);
		return panels;
	}

	private JMenuBar createJMenuBar() {
		JMenuBar jMenuBar = new JMenuBar();

		JMenu file = new JMenu("File");
		jMenuBar.add(file);

		JMenuItem newItem = new JMenuItem("Open image");
		newItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				JFileChooser fileChooser = new JFileChooser();
				int returnVal = fileChooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					drawing.openImage(file);
				}
			}
		});
		file.add(newItem);
		JMenuItem scaleItem = new JMenuItem("Set scale");
		scaleItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null, createScaleMenu(), "Setting Scale",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				if(result == JOptionPane.OK_OPTION) {
					drawing.setScale(k[0], k[1]);
					drawing.setCenter(k[2], k[3]);
				} else
				if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION) {

				}
			}
		});
		file.add(scaleItem);
		JMenuItem saveItem = new JMenuItem("Save XML");
		saveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null, createTextArea(), "Coordinate",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				if(result == JOptionPane.OK_OPTION) {

				} else
				if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION) {

				}
			}
		});
		file.add(saveItem);

		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				System.exit(0);
			}
		});
		file.add(exitItem);

		JMenu help = new JMenu("Help");
		jMenuBar.add(help);

		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				JOptionPane.showMessageDialog(new JFrame(), "Sergey Ivanov\n2016");
			}
		});
		help.add(aboutItem);

		return jMenuBar;
	}
}
